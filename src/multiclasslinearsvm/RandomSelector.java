/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiclasslinearsvm;

/**
 *
 * @author Siamul Karim Khan
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Siamul Karim Khan
 */
public class RandomSelector {
    public static double[][][] separateSet(double[][] table)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=0; i<table.length; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        int trainSetIndex = ((table.length - 1)*8)/10;
        Vector<double[]> trainSet = new Vector<>();
        Vector<double[]> testSet = new Vector<>();
        for(int i = 0; i<trainSetIndex; i++)
        {
            trainSet.add(table[list.get(i)]);
        }
        for(int i = trainSetIndex; i<list.size(); i++)
        {
            testSet.add(table[list.get(i)]);
        }
        double[][][] retVal = new double[2][][];
        retVal[0] = new double[trainSet.size()][];
        retVal[1] = new double[testSet.size()][];
        retVal[0] = trainSet.toArray(retVal[0]);
        retVal[1] = testSet.toArray(retVal[1]);
        return retVal;
    }
}

