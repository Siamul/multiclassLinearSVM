/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiclasslinearsvm;
import libsvm.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author Siamul Karim Khan
 */
public class MulticlassLinearSVM {

    /**
     * @param args the command line arguments
     */
    static Map<Integer, String> classMap = new HashMap<>();
    public static void print2Darr( double[][] arr ) 
    {
        for(int i = 0; i<arr.length; i++)
        {
            for(int j = 0; j<arr[i].length; j++)
            {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    static svm_parameter parameter = new svm_parameter();
    static void initializeParam()
    {
        parameter.svm_type = svm_parameter.C_SVC;
        parameter.kernel_type = svm_parameter.LINEAR;
        parameter.degree = 1;
        parameter.gamma = 0;
        parameter.coef0 = 0;
        parameter.cache_size = 100;
        parameter.eps = 1e-3;
        parameter.C = 1;
        parameter.nr_weight = 0;
        parameter.weight_label = new int[0];
        parameter.weight = new double[0];
        parameter.nu = 0.5;
        parameter.p = 0.1;
        parameter.shrinking = 0;
        parameter.probability = 0;
    }

    static svm_problem createProblem(double[][] set, svm_node[][] list, double plusOne, double minusOne)
    {
        svm_problem svmprob = new svm_problem();
        if(minusOne < 0) //One vs. All
        {
            svmprob.l = list.length;
            svmprob.x = list;
            svmprob.y = new double[list.length];
            for(int i = 0; i<set.length; i++)
            {
                if(set[i][set[0].length - 1] == plusOne)
                {
                    svmprob.y[i] = +1.0;
                }
                else
                {
                    svmprob.y[i] = -1.0;
                }
            }
            return svmprob;
        }
        else //One vs. One
        {
            ArrayList<svm_node[]> rowsNeeded = new ArrayList<>();
            ArrayList<Double> label = new ArrayList<>();
            for(int i = 0; i<set.length; i++)
            {
                if(set[i][set[0].length - 1] == plusOne)
                {
                    rowsNeeded.add(list[i]);
                    label.add(+1.0);
                }
                else if(set[i][set[0].length - 1] == minusOne)
                {
                    rowsNeeded.add(list[i]);
                    label.add(-1.0);
                }
            }
            svmprob.x = new svm_node[rowsNeeded.size()][];
            svmprob.y = new double[label.size()];
            for(int i = 0; i<svmprob.x.length; i++)
            {
                svmprob.x[i] = rowsNeeded.get(i);
                svmprob.y[i] = label.get(i);
            }
            svmprob.l = svmprob.x.length;
        }
        return svmprob;
    }
    
    public static double oneVsRest(double[][] set, svm_node[][] list, svm_node[] row, double[] label)
    {
        double maxLabel = -1;
        double maxScore = -99999999;
        svm_problem problem;
        svm_model model;
        for(int i = 0; i<label.length; i++)
        {
            problem = createProblem(set, list, label[i], -1);
            model = svm.svm_train(problem, parameter);
            double[] value = new double[1];
            svm.svm_predict_values(model, row, value);
            if(maxScore < value[0])
            {
                maxLabel = label[i];
                maxScore = value[0];
            }
        }
        return maxLabel;
    }
    
    public static double oneVsOne(double[][] set, svm_node[][] list, svm_node[] row, double[] label)
    {
        int[] votes = new int[label.length];
        svm_problem problem;
        svm_model model;
        for(int i = 0; i<label.length; i++)
        {
            for(int j = i+1; j<label.length; j++)
            {
                problem = createProblem(set, list, label[i], label[j]);
                model = svm.svm_train(problem, parameter);
                double pVal = svm.svm_predict(model, row);
                if(pVal < 0)
                {
                    votes[j]++;
                }
                else
                {
                    votes[i]++;
                }
            }
        }
        double maxLabel = label[0];
        int maxVotes = votes[0];
        for(int i = 1; i<votes.length; i++)
        {
            if(votes[i] > maxVotes)
            {
                maxVotes = votes[i];
                maxLabel = label[i];
            }
        }
        return maxLabel;
    }
    
    
    public static void main(String[] args) {
        svm_print_interface svmPrint = new svm_print_interface() {
            @Override
            public void print(String string) {
            }
        };
        svm.svm_set_print_string_function(svmPrint);
        initializeParam();
        // TODO code application logic here
        String csvFile = "IRISdata.csv";
        //String csvFile = "pima-indians-diabetes.csv";
        String rline;
        String[] line;
        Vector<Vector<String> > list = new Vector<>();
        Vector<String> temp;
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while((rline = br.readLine()) != null)
            {
                line = rline.split(",");
                temp = new Vector<String>();
                for(int i = 0; i<line.length; i++)
                {
                    temp.add(line[i]);
                }
                list.add(temp);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        String[][] attrList = new String[list.size()][];
        Vector<String> temp2;
        for(int i = 0; i<attrList.length; i++)
        {
            temp2 = list.get(i);
            attrList[i] = new String[temp2.size()];
            for(int j = 0; j<attrList[i].length; j++)
            {
                attrList[i][j] = temp2.get(j);
            }
        }       
        classMap.put(0, attrList[0][attrList[0].length - 1]);
        attrList[0][attrList[0].length - 1] = "0";
        int classif = 1;
        for(int i = 1; i<attrList.length; i++)
        {
            if(classMap.containsValue(attrList[i][attrList[i].length - 1]))
            {
                for(Map.Entry<Integer, String> entry : classMap.entrySet())
                {
                    if(entry.getValue().compareTo(attrList[i][attrList[i].length - 1]) == 0)
                    {
                        attrList[i][attrList[i].length - 1] = String.valueOf(entry.getKey());
                        break;
                    }
                }
            }
            else
            {
                classMap.put(classif, attrList[i][attrList[i].length - 1]);
                attrList[i][attrList[i].length - 1] = String.valueOf(classif++);
            }
        }
        //print2Darr(attrList);
        double[][] attrDList = new double[attrList.length][attrList[0].length];
        for(int i = 0; i<attrList.length; i++)
        {
            for(int j = 0; j<attrList[i].length; j++)
            {
                attrDList[i][j] = Double.valueOf(attrList[i][j]);
            }
        }
        //print2Darr(attrDList);
        double[] max = new double[attrDList[0].length - 1];
        double[] min = new double[attrDList[0].length - 1];
        for(int i = 0; i<max.length; i++)
        {
            max[i] = -9999999;
            min[i] = 9999999;
        }
        for(int i = 0; i<attrDList.length; i++)
        {
            for(int j = 0; j<max.length; j++)
            {
                if(attrDList[i][j] > max[j])
                {
                    max[j] = attrDList[i][j];
                }
                if(attrDList[i][j] < min[j])
                {
                    min[j] = attrDList[i][j];
                }
            }
        }
        for(int i = 0; i<attrDList.length; i++)
        {
            for(int j = 0; j<attrDList[i].length - 1; j++)
            {
                attrDList[i][j] = (((2 * attrDList[i][j])/(max[j] - min[j])) - ((max[j] + min[j])/(max[j] - min[j])));
            }
        }
        //print2Darr(attrDList);
        double[][][] setSel = RandomSelector.separateSet(attrDList);
        double[][] trainSet = setSel[0];
        double[][] testSet = setSel[1];
//        ArrayList<svm_node> trainNodeList = new ArrayList<>();
//        ArrayList<svm_node>testNodeList = new ArrayList<>();
        svm_node[][] trainNodeList = new svm_node[trainSet.length][trainSet[0].length - 1];
        svm_node[][] testNodeList = new svm_node[testSet.length][testSet[0].length - 1];
        svm_node n;
        for(int i = 0; i<trainSet.length; i++)
        {
            for(int j = 0; j<trainSet[i].length - 1; j++)
            {
                n = new svm_node();
                n.index = j;
                n.value = trainSet[i][j];
                trainNodeList[i][j] = n;
            }
        }
        for(int i = 0; i<testSet.length; i++)
        {
            for(int j = 0; j<testSet[i].length - 1; j++)
            {
                n = new svm_node();
                n.index = j;
                n.value = testSet[i][j];
                testNodeList[i][j] = n;
            }
        }
        System.out.println("============================");
        System.out.println("Output for One vs. Rest");
        System.out.println("============================");
        double correct = 0;
        double[] label = new double[classMap.keySet().size()];
        int index = 0;
        for(Integer key : classMap.keySet())
        {
            label[index++] = key;
        }
        //System.out.println(testNodeList.length + " " + testSet.length);
       // System.out.println(testNodeList[0].length + " " + testSet[0].length);
//        for(int i = 0; i<label.length; i++)
//        {
//            System.out.print(label[i] + " ");
//        }
//        System.out.println();
        for(int i = 0; i<testNodeList.length; i++)
        {
            double resultlabel = oneVsRest(trainSet, trainNodeList, testNodeList[i], label);
//            System.out.println("=======================");
//            System.out.println(resultlabel);
//            System.out.println(testSet[i][testSet[0].length - 1]);
//            System.out.println("=======================");
            if(testSet[i][testSet[0].length - 1] == resultlabel)
            {
                correct++;
            }
        }
        System.out.println("Accuracy: " + (correct/(double)testNodeList.length));
        System.out.println("============================");
        System.out.println("============================");
        System.out.println("Output for One vs. One");
        System.out.println("============================");
        correct = 0;
        for(int i = 0; i<testNodeList.length; i++)
        {
            double resultlabel = oneVsOne(trainSet, trainNodeList, testNodeList[i], label);
            if(testSet[i][testSet[0].length - 1] == resultlabel)
            {
                correct++;
            }
        }
        System.out.println("Accuracy: " + (correct/(double)testNodeList.length));
        System.out.println("============================");
    }    
}
